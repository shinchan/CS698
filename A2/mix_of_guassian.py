#!/bin/python3
import numpy as np
from scipy import stats
import heapq
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from utility import sigmoid, batch

def mixGaussian(X1, X2, printprmtr = False):
  assert X1.shape[1] == X2.shape[1], "X1 and X2 should have the same number of columns"
  p1 = X1.shape[0]/(X1.shape[0] + X2.shape[0])
  mean1 = np.mean(X1, axis=0)
  mean2 = np.mean(X2, axis=0)    
  Cov = (np.cov(X1.T)*X1.shape[0] + np.cov(X2.T)*X2.shape[0])/(X1.shape[0] + X2.shape[0])
  Covinv = np.linalg.inv(Cov)
  w = np.matmul(Covinv, (mean1-mean2))
  w0 = -0.5*np.matmul(np.matmul(mean1, Covinv), mean1) + \
        0.5*np.matmul(np.matmul(mean2, Covinv), mean2) + np.log(p1/(1-p1))
  return np.concatenate((w,[w0]), axis=0)

def mixGaussian_cross_validation():
  accuracy_lst = []
  for test_set_idx in range(1, 11):
    X = np.array([], dtype=int).reshape(0, 64)
    t = np.array([]).reshape(0, 1)
    for rg in [range(1, test_set_idx), range(test_set_idx+1, 11)]:
      for train_set_idx in rg:
        xbatch, tbatch = batch(train_set_idx)
        X = np.concatenate((X, xbatch), axis=0)
        t = np.concatenate((t, tbatch), axis=0)
    X5 = np.array([u for (u, v) in zip(X, t) if v == [5]])
    X6 = np.array([u for (u, v) in zip(X, t) if v == [6]])
    w = mixGaussian(X5, X6)
    X_test, t_test = batch(test_set_idx)
    X_test = np.concatenate((X_test, np.ones(X_test.shape[0]).reshape(-1, 1)), axis=1)
    y_test = sigmoid(np.matmul(X_test, w))
    lst = [np.abs(u-int(v[0]<6))<=0.5 for (u, v) in zip(y_test, t_test)]
    accuracy = sum(lst)/len(lst)
    accuracy_lst += [accuracy]
  return accuracy_lst

print(mixGaussian_cross_validation())
