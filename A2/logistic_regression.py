#!/bin/python3
import numpy as np
from scipy import stats
import heapq
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from utility import sigmoid, batch
  
def logisticReg(X1, X2, regularization = 0):
  assert X1.shape[1] == X2.shape[1], "X1 and X2 should have the same number of columns"
  m1 = X1.shape[0]
  m2 = X2.shape[0]
  n = X1.shape[1]
  X1 = np.concatenate((X1, np.ones(m1).reshape(-1, 1)), axis=1)
  X2 = np.concatenate((X2, np.ones(m2).reshape(-1, 1)), axis=1)
  X = np.concatenate((X1, X2), axis=0)
  w = 0.001*(np.random.rand(X1.shape[1])-0.5)
  for i in range(10):
    a = sigmoid(np.matmul(X, w))
    grad = np.matmul(a[0:m1]-1, X1) + np.matmul(a[m1:], X2)
    hess = np.matmul(np.matmul(X.T, np.diag(a*(1-a))), X) + regularization*np.identity(n+1)
    dw = -np.matmul(np.linalg.inv(hess), grad)
    w += dw
  return w;

def logisticReg_cross_validation(regularization):
  accuracy_lst = []
  for test_set_idx in range(1, 11):
    X = np.array([], dtype=int).reshape(0, 64)
    t = np.array([]).reshape(0, 1)
    for rg in [range(1, test_set_idx), range(test_set_idx+1, 11)]:
      for train_set_idx in rg:
        xbatch, tbatch = batch(train_set_idx)
        X = np.concatenate((X, xbatch), axis=0)
        t = np.concatenate((t, tbatch), axis=0)
    X5 = np.array([u for (u, v) in zip(X, t) if v == [5]])
    X6 = np.array([u for (u, v) in zip(X, t) if v == [6]])
    w = logisticReg(X5, X6, regularization)
    X_test, t_test = batch(test_set_idx)
    X_test = np.concatenate((X_test, np.ones(X_test.shape[0]).reshape(-1, 1)), axis=1)
    y_test = sigmoid(np.matmul(X_test, w))
    lst = [np.abs(u-int(v[0]<6))<=0.5 for (u, v) in zip(y_test, t_test)]
    accuracy = sum(lst)/len(lst)
    accuracy_lst += [accuracy]
  return accuracy_lst

accuracy_lst = logisticReg_cross_validation(1)
print(accuracy_lst)

# train on the entire dataset
X = np.array([], dtype=int).reshape(0, 64)
t = np.array([]).reshape(0, 1)
for train_set_idx in range(1, 11):
  xbatch, tbatch = batch(train_set_idx)
  X = np.concatenate((X, xbatch), axis=0)
  t = np.concatenate((t, tbatch), axis=0)
X5 = np.array([u for (u, v) in zip(X, t) if v == [5]])
X6 = np.array([u for (u, v) in zip(X, t) if v == [6]])
w = logisticReg(X5, X6, 1)
print(w)

#X, t = batch(2)
#X5 = np.array([u for (u, v) in zip(X, t) if v == [5]])
#X6 = np.array([u for (u, v) in zip(X, t) if v == [6]])
#logisticReg(X5, X6, 0.01)


  

































