#!/bin/python3
import csv
import scipy as sp
from scipy import linalg
import numpy as np
from scipy import stats
import heapq
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def batch(i):
  with open('fData'+str(i)+'.csv', 'r') as data_file, open('fLabels'+str(i)+'.csv', 'r') as label_file:
    data_reader = csv.reader(data_file)
    label_reader = csv.reader(label_file)
    data = []
    labels = []
    for (data_row, label_row) in zip(data_reader, label_reader):
      data += [[np.float(x) for x in data_row]]
      labels += [[np.float(x) for x in label_row]]
    return (np.array(data), np.array(labels))

def linregr_cross_validation(regularization):
  RMS_error_lst = []
  for test_set_idx in range(1, 11):
    A = np.array([]).reshape(0, 2)
    b = np.array([]).reshape(0, 1)
    for rg in [range(1, test_set_idx), range(test_set_idx+1, 11)]:
      for train_set_idx in rg:
        x, y = batch(train_set_idx)
        A = np.concatenate((A, x), axis=0)
        b = np.concatenate((b, y), axis=0)
    A = np.concatenate((A, np.ones(A.shape[0]).reshape(-1, 1)), axis=1)
    w = linregr(A, b, regularization)
    A_test, b_test = batch(test_set_idx)
    A_test = np.concatenate((A_test, np.ones(A_test.shape[0]).reshape(-1, 1)), axis=1)    
    RMS_error_lst += [sp.linalg.norm(np.matmul(A_test,w)-b_test, ord=2)]
  return sum(RMS_error_lst)/len(RMS_error_lst)

def linregr(A, b, regularization):
  #print(regularization)
  m, n = A.shape
  assert m == b.shape[0], "A and b must have the same number of rows"
  w = np.matmul(np.linalg.inv(np.matmul(A.transpose(), A) 
        + regularization*np.eye(n)), np.matmul(A.transpose(),b))
  return w

RMS_errors = [linregr_cross_validation(0.1*2*lbda) for lbda in range(0, 41)]
#print(RMS_errors)
matplotlib.rc('text', usetex=True)
plt.plot(np.linspace(0, 4.0, 41), RMS_errors)
plt.xlabel(r"$\lambda$")
plt.ylabel(r"RMS error")
plt.title(r"Cross Validation for Different $\lambda$ (RMS error)")
plt.show()


regularization = 0.1*RMS_errors.index(min(RMS_errors))
print(regularization)
A = np.array([]).reshape(0, 2)
b = np.array([]).reshape(0, 1)
for train_set_idx in range(1, 11):
  x, y = batch(train_set_idx)
  A = np.concatenate((A, x), axis=0)
  b = np.concatenate((b, y), axis=0)  
A = np.concatenate((A, np.ones(A.shape[0]).reshape(-1, 1)), axis=1)
w = linregr(A, b, regularization)
print(w)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(A[:,0], A[:,1], b)
x = np.linspace(0, 16, 11)
y = np.linspace(0, 16, 11)
z = np.concatenate((np.array([[u,v] for v in y for u in x]),np.ones(11*11).reshape(-1,1)), axis = 1)
z = np.matmul(z, w).reshape(11,11)
x,y = np.meshgrid(x,y)
ax.plot_wireframe(x, y, z, color="green")
ax.set_xlabel(r"$x_1$")
ax.set_ylabel(r"$x_2$")
ax.set_zlabel(r"$z$")
plt.title("Linear Regression")
plt.show()
