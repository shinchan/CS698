#!/bin/python3
import csv
import scipy as sp
import numpy as np
from scipy import stats
from scipy import linalg
import heapq
import matplotlib.pyplot as plt

class Knn:
  """Finds the k nearest neighbours of x, locking semantics are NOT implemented.
  """
  def __init__(self, k, x):
    assert k >= 1, "k of K-nearest-neighbours must larger or equal to 1"
    self.k = k
    self.x = x
    self.neighbours = [] 
    self.cnt = 0 # count how many data points have been added so far
  def add_train_data(self, train_data_iter):
    for i, train_data in enumerate(train_data_iter):
      self.cnt += 1
      #print(i, self.neighbours)
      priority = -sp.linalg.norm(self.x - train_data[0], ord=1)
      #print(self.cnt, i, priority)
      if len(self.neighbours) == self.k:
        heapq.heappushpop(self.neighbours, (priority, np.random.uniform(), self.cnt, train_data))
        # self.cnt prevent ties in priority
      else:
        heapq.heappush(self.neighbours, (priority, np.random.uniform(), self.cnt, train_data))
  def get_target(self):
    """
    Returns:
      scipy.array: target obtained by the K-nearest-neighbours algorithm
    """
    targets = [t[3][1] for t in self.neighbours]
    #print(self.neighbours[0][0], self.cnt)
    #print("targets", targets)
    return stats.mode(targets)

def batch(i):
  with open('data'+str(i)+'.csv', 'r') as data_file, open('labels'+str(i)+'.csv', 'r') as label_file:
    data_reader = csv.reader(data_file)
    label_reader = csv.reader(label_file)
    for (data_row, label_row) in zip(data_reader, label_reader):
      yield np.array([int(x) for x in data_row]), np.array([int(x) for x in label_row]) 
    
def knn_cross_validation(knn_k):
  print(knn_k)
  accuracy_lst = []
  for test_set_idx in range(1, 11):
    correct_cnt = 0
    for cnt,(x,y) in enumerate(batch(test_set_idx)):
      knn = Knn(knn_k, x)
      for rg in [range(1, test_set_idx), range(test_set_idx+1, 11)]:
        for train_set_idx in rg:
          knn.add_train_data(batch(train_set_idx))
      if knn.get_target()[0] == y:
        correct_cnt += 1
    accuracy_lst += [correct_cnt/cnt]
  #print(accuracy_lst)
  return sum(accuracy_lst)/len(accuracy_lst)

accuracy = [knn_cross_validation(k) for k in range(1, 31)]
print(accuracy)
plt.plot(range(1, 31), accuracy)
plt.xlabel("k")
plt.ylabel("accuracy")
plt.title("cross validation for different k in KNN (L1 norm)")
plt.show()

#[0.7300000000000001, 0.7354545454545456, 0.7827272727272727, 0.78, 0.7854545454545456, 0.7945454545454546, 0.7918181818181818, 0.7945454545454546, 0.8018181818181818, 0.8018181818181818, 0.8009090909090908, 0.8018181818181818, 0.8054545454545454, 0.8063636363636364, 0.7954545454545454, 0.7990909090909091, 0.7945454545454547, 0.8099999999999999, 0.8099999999999999, 0.8172727272727272, 0.8209090909090907, 0.8227272727272726, 0.8227272727272729, 0.82, 0.8154545454545452, 0.820909090909091, 0.8172727272727274, 0.8209090909090907, 0.8227272727272726, 0.8218181818181819]
