#!/bin/python3
import csv
import numpy as np

def sigmoid(x):
  return 1 / (1 + np.exp(-x))

def trainseq(i):
  with open('trainDataSeq'+str(i)+'.csv', 'r') as data_file, open('trainLabelSeq'+str(i)+'.csv', 'r') as label_file:
    data_reader = csv.reader(data_file)
    label_reader = csv.reader(label_file)
    data = []
    labels = []
    for (data_row, label_row) in zip(data_reader, label_reader):
      data += [[float(x) for x in data_row]]
      labels += [[int(x) for x in label_row]]
    return (np.array(data), np.array(labels))

def testseq(i):
  with open('testDataSeq'+str(i)+'.csv', 'r') as data_file, open('testLabelSeq'+str(i)+'.csv', 'r') as label_file:
    data_reader = csv.reader(data_file)
    label_reader = csv.reader(label_file)
    data = []
    labels = []
    for (data_row, label_row) in zip(data_reader, label_reader):
      data += [[float(x) for x in data_row]]
      labels += [[int(x) for x in label_row]]
    return (np.array(data), np.array(labels))
