#!/bin/python3
import numpy as np
from scipy import stats
import heapq
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from utility import sigmoid, trainseq, testseq


X = np.array([], dtype=float).reshape(0, 100, 2)
z = np.array([], dtype=int).reshape(0, 100, 1)
for train_seq_idx in range(1,6):
  xseq, zseq = trainseq(train_seq_idx)
  xseq = xseq.reshape(1, 100, 2)
  zseq = zseq.reshape(1, 100, 1)
  X = np.concatenate((X, xseq), axis=0)
  z = np.concatenate((z, zseq), axis=0)    
p_init = np.array([len([t for v in z if v[0][0] == t])/X.shape[0] 
          for t in range(1, 4)])
print("initial probability: \n", p_init)

p_tran = np.zeros((3, 3), dtype=float)
for i in range(0, 3):
  for j in range(0, 3):
    p_tran[i][j] = len([0 for v in z for k in range(0, 99) 
                        if v[k][0] == i+1 and  v[k+1][0] == j+1
                        ])
p_tran = p_tran/\
         np.linalg.norm(p_tran, ord=1, axis=1).reshape(-1, 1)
print("transition probability: \n", p_tran)

def mixGaussian(X0, X1, X2, printprmtr = False):
  assert X0.shape[1] == X1.shape[1] and X0.shape[1] == X2.shape[1]\
         , "X0, X1 and X2 should have the same number of columns"
  p = []
  p += [X0.shape[0]/(X0.shape[0] + X1.shape[0] + X2.shape[0])]
  p += [X1.shape[0]/(X0.shape[0] + X1.shape[0] + X2.shape[0])]
  p += [1 - p[0] - p[1]]
  p = np.array(p)
  mean = np.array([np.mean(X0, axis=0), np.mean(X1, axis=0), 
                   np.mean(X2, axis=0)])
  Cov = (np.cov(X0.T)*X0.shape[0] + np.cov(X1.T)*X1.shape[0]
         + np.cov(X2.T)*X2.shape[0])/(X0.shape[0] + X1.shape[0] 
         + X2.shape[0])
  Covinv = np.linalg.inv(Cov)
  W = np.matmul(mean, Covinv)
  W0 = -0.5*np.diag(np.matmul(np.matmul(mean, Covinv), mean.T)) \
       + np.log(p)
#  print(W.shape)
#  print(W0.shape)
  if printprmtr:
    print("mean of each class: \n", mean)
    print("covariance: \n", Cov)
  W = np.concatenate((W, W0.reshape(-1, 1)), axis=1)
  return (W, mean, Cov)

X0 = np.array([u for (u, v) in 
               zip(X.reshape(-1, 2), z.reshape(-1, 1)) 
               if v[0] == 1])
X1 = np.array([u for (u, v) in 
               zip(X.reshape(-1, 2), z.reshape(-1, 1)) 
               if v[0] == 2])
X2 = np.array([u for (u, v) in 
               zip(X.reshape(-1, 2), z.reshape(-1, 1)) 
               if v[0] == 3])
W, mean, Cov = mixGaussian(X0, X1, X2, True)

#print(X0)
print("mixture of Gaussian weight for each class: \n", W)

def monitor_hmm(p_init, p_tran, mean, Cov, xseq):
  alpha = []
  for t in range(0, len(xseq)):
    if t == 0:
      prior = p_init
    else:
      prior = np.matmul(alpha[t-1], p_tran)
    likelihood = np.exp(-0.5*np.diag(np.matmul(np.matmul(xseq[t]-mean,\
                        np.linalg.inv(Cov)), (xseq[t]-mean).T)))
    post = likelihood * prior
    post = post/np.linalg.norm(post,ord=1)
    alpha += [post]
#    print(post)
  return np.argmax(alpha, axis = 1)

def monitor_mixGaussian(W, xseq):
  xseq = np.concatenate((xseq, np.ones((xseq.shape[0],1), dtype=float))\
                        , axis=1)
  return np.argmax(np.matmul(xseq, W.T), axis = 1)
  
def viterbi(p_init, p_tran, mean, Cov, xseq):
  mlseq = []
  for t in range(0, len(xseq)):
    likelihood = np.exp(-0.5*np.diag(np.matmul(np.matmul(xseq[t]-mean,\
                        np.linalg.inv(Cov)), (xseq[t]-mean).T)))
    if t == 0:
      f = likelihood * p_init
      f = f/np.linalg.norm(f,ord=1)
    else:
      F = likelihood * (p_tran.T * f).T
      f = np.max(F, axis=0)
      # 1-norm doesn't necessarily equal to 1, but normalize it anyway
      f = f/np.linalg.norm(f,ord=1)
      #print(f)
      mlseq += [np.argmax(F, axis=0)]
  traceback = np.argmax(f) 
  mlseq += [traceback]
  for i in range(len(mlseq)-2, -1, -1):
    mlseq[i] = mlseq[i][traceback]
    traceback = mlseq[i]
  print(mlseq)
  print(len(mlseq))
  #mlseq = np.append(mlseq, [np.argmax(f)])

  return np.array(mlseq)

X_test = np.array([], dtype=float).reshape(0, 100, 2)
z_test = np.array([], dtype=int).reshape(0, 100, 1)
for test_seq_idx in range(1,6):
  xseq, zseq = testseq(test_seq_idx)
  xseq = xseq.reshape(1, 100, 2)
  zseq = zseq.reshape(1, 100, 1)
  X_test = np.concatenate((X_test, xseq), axis=0)
  z_test = np.concatenate((z_test, zseq), axis=0)   
#print(X_test)
accuracy_hmm = [np.sum(v.reshape(-1) == 1+monitor_hmm(p_init, p_tran,\
                mean, Cov, u))/len(v) for (u, v) in zip(X_test, z_test)]
accuracy_mixGaussian = [np.sum(v.reshape(-1) == 1+monitor_mixGaussian\
                        (W, u))/len(v) for (u, v) in zip(X_test, z_test)]
accuracy_viterbi = [np.sum(v.reshape(-1) == 1+viterbi(p_init,p_tran,\
                    mean, Cov, u))/len(v) for (u, v) in zip(X_test,
                    z_test)]
#accuracy_hmm = [v == monitor_hmm(p_init, p_tran, mean, Cov, u) for (u, v) in zip(X_test, z_test)]
print("HMM accuracy on 5 test seq (average and respective):")
print(sum(accuracy_hmm)/len(accuracy_hmm))
print(accuracy_hmm)
print("mixture of Gaussian accuracy:")
print(sum(accuracy_mixGaussian)/len(accuracy_mixGaussian))
print(accuracy_mixGaussian)
print("viterbi accuracy:")
print(sum(accuracy_viterbi)/len(accuracy_viterbi))
print(accuracy_viterbi)
#print(1+monitor_mixGaussian(W, X_test[4]))
#print(X_test[4])
#print(z_test[4])


plt.plot(X0[:, 0], X0[:, 1], '*')
plt.plot(X1[:, 0], X1[:, 1], 'o')
plt.plot(X2[:, 0], X2[:, 1], '+')
plt.show()
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
