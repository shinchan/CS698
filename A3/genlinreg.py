#!/bin/python3
import csv
import scipy as sp
from scipy import linalg
import numpy as np
from scipy import stats
import heapq
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import time

def batch(i):
  with open('fData'+str(i)+'.csv', 'r') as data_file, open('fLabels'+str(i)+'.csv', 'r') as label_file:
    data_reader = csv.reader(data_file)
    label_reader = csv.reader(label_file)
    data = []
    labels = []
    for (data_row, label_row) in zip(data_reader, label_reader):
      data += [[np.float(x) for x in data_row]]
      labels += [[np.float(x) for x in label_row]]
    return (np.array(data), np.array(labels))

def linregr_cross_validation(degree):
  starttime = time.time()
  MS_error_lst = []
  for test_set_idx in range(1, 11):
    A = np.array([]).reshape(0, (degree+1)*(degree+2)//2)
    b = np.array([]).reshape(0, 1)
    for rg in [range(1, test_set_idx), range(test_set_idx+1, 11)]:
      for train_set_idx in rg:
        x, y = batch(train_set_idx)
        phix = tf(x, degree)
        A = np.concatenate((A, phix), axis=0)
        b = np.concatenate((b, y), axis=0)
    w = linregr(A, b, regularization = 2)
    A_test, b_test = batch(test_set_idx)
    A_test = tf(A_test, degree)
    MS_error_lst += [sp.linalg.norm(np.matmul(A_test,w)-b_test, ord=2)**2]
  print(time.time()-starttime)
  return sum(MS_error_lst)/len(MS_error_lst)

def linregr(A, b, regularization):
  #print(regularization)
  m, n = A.shape
  assert m == b.shape[0], "A and b must have the same number of rows"
  w = np.matmul(np.linalg.inv(np.matmul(A.transpose(), A) 
        + regularization*np.eye(n)), np.matmul(A.transpose(),b))
  return w

def tf(x_batch, d):
  assert d >= 1, "feature degree must be greater than 1"
  phix_batch = []
  for x in x_batch:
    phix = [x[0]**j * x[1]**(i-j) for i in range(d, -1, -1) for j in range(i, -1, -1)]
    phix_batch += [phix]
  return np.array(phix_batch)

MS_errors = [linregr_cross_validation(degree) for degree in range(1, 4+1)]
#print(MS_errors)
plt.plot(np.linspace(1, 4, 4), MS_errors)
plt.xlabel("degree")
plt.ylabel("mean square error")
plt.title("10-fold Cross Validation (Generalized Linear Regression)")
plt.show()
