#!/bin/python3
import csv
import scipy as sp
from scipy import linalg
import numpy as np
from scipy import stats
import heapq
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import time

def batch(i):
  with open('fData'+str(i)+'.csv', 'r') as data_file, open('fLabels'+str(i)+'.csv', 'r') as label_file:
    data_reader = csv.reader(data_file)
    label_reader = csv.reader(label_file)
    data = []
    labels = []
    for (data_row, label_row) in zip(data_reader, label_reader):
      data += [[np.float(x) for x in data_row]]
      labels += [[np.float(x) for x in label_row]]
    return (np.array(data), np.array(labels))

def kernelregr_cross_validation(var, degree):
  starttime = time.time()
  MS_error_lst = []
  for test_set_idx in range(1, 11):
    A = np.array([]).reshape(0, 2)
    b = np.array([]).reshape(0, 1)
    for rg in [range(1, test_set_idx), range(test_set_idx+1, 11)]:
      for train_set_idx in rg:
        x, y = batch(train_set_idx)
        A = np.concatenate((A, x), axis=0)
        b = np.concatenate((b, y), axis=0)
    #K = kernel_identity(A, A)
    K = kernel_poly(A, A, degree)
    #K = kernel_exp(A, A, kern_var)
    A_test, b_test = batch(test_set_idx)
    b_pred = np.matmul(np.matmul(kernel_poly(A_test, A, degree), np.linalg.inv(K + var*np.identity(K.shape[0]))), b)
    #print(b_pred)
    #print(b_test)
    MS_error_lst += [sp.linalg.norm(b_pred-b_test, ord=2)**2]
  print(time.time()-starttime)
  return sum(MS_error_lst)/len(MS_error_lst)

def kernel_identity(X1, X2):
  return np.matmul(X1, X2.T)

def kernel_poly(X1, X2, degree):
  return (np.matmul(X1, X2.T) + 1)**degree

def kernel_exp(X1, X2, var):
  t1 = sp.linalg.norm(X1, axis = 1, ord=2)**2
  t1 = t1.reshape(t1.shape[0], 1)
  t2 = np.matmul(X1, X2.T)
  t3 = sp.linalg.norm(X2, axis = 1, ord=2)**2
  return np.exp(-(t1 - 2*t2 + t3)/(2*var))

MS_errors = [kernelregr_cross_validation(1, d) for d in range(1, 5)]
#MS_errors = [kernelregr_cross_validation(1, std**2) for std in range(1, 7)]
#print(kernelregr_cross_validation(1))
#plt.plot(np.linspace(1, 6, 6), MS_errors)
#plt.xlabel("standard deviation")
#plt.ylabel("mean square error")
#plt.title("10-fold Cross Validation (Gaussian Process Regression, Gaussian Kernel)")
#plt.show()
